/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.sqlbuilder.dml;

import com.cicadascms.support.datamodel.sqlbuilder.SqlBuilder;


/**
 * @author Jin
 * @date 2021-02-01 21:28:13
 * @description: 删除表数据
 */
public interface DeleteDataSqlBuilder<T extends DeleteDataSqlBuilder> extends SqlBuilder {

    T delete(String conditionField, Object conditionValue);

    T tableName(String tableName);

}
