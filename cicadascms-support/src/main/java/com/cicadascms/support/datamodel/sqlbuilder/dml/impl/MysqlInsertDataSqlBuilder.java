/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.sqlbuilder.dml.impl;

import com.cicadascms.support.datamodel.sqlbuilder.AbstractSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.dml.InsertDataSqlBuilder;

import java.util.List;


public class MysqlInsertDataSqlBuilder extends AbstractSqlBuilder<MysqlInsertDataSqlBuilder> implements InsertDataSqlBuilder<MysqlInsertDataSqlBuilder> {

    private final static String INSERT_DATA_BEGIN = "insert into `{table}` set ";

    @Override
    public MysqlInsertDataSqlBuilder insert(List<String> fields, List<Object> paramValues) {
        StringBuilder sqlBody = new StringBuilder();
        for (int i = 0; i < fields.size(); i++) {
            sqlBody.append(" `").append(fields.get(i)).append("`= ");
            Object paramValue = paramValues.get(i);
            if (paramValue instanceof String) {
                sqlBody.append("'");
                sqlBody.append(paramValues.get(i));
                sqlBody.append("'");
            } else {
                sqlBody.append(paramValue);
            }
            if (i >= fields.size() - 1) {
                sqlBody.append("; ");
            } else {
                sqlBody.append(", ");
            }
        }
        setSqlBody(sqlBody.toString());
        return this;
    }

    @Override
    public MysqlInsertDataSqlBuilder tableName(String tableName) {
        clearSql();
        setTableName(tableName);
        setSqlHead(INSERT_DATA_BEGIN);
        return this;
    }

    @Override
    public String buildSql() {
        return build();
    }
}
