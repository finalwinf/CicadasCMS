package com.cicadascms.service.front.logic.controller.api;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "前台内容接口")
@RestController
@RequestMapping("/api/content")
public class ApiContentController {

    @GetMapping("/")
    public String index() {
        return "ok";
    }

}
