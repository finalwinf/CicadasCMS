package com.cicadascms.service.front.logic.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.ContentDO;
import com.cicadascms.service.front.logic.vo.ContentVO;

import java.util.Map;

/**
 * <p>
 * 内容 服务类
 * </p>
 *
 * @author Jin
 * @since 2021-01-26
 */
public interface IContentService extends IService<ContentDO> {

    /**
     * 查询内容分页
     *
     * @param pageNumber
     * @param channelId
     * @return
     */
    IPage<ContentVO> findByPageNumberAndChannelId(Integer pageNumber, Integer channelId);

    /**
     * 查询扩展表字段内容
     *
     * @param tableName
     * @param contentId
     * @return
     */
    Map<String, Object> findByTableNameAndContentId(String fields,String tableName, Integer contentId);

}
