package com.cicadascms.service.system.logic.dto;


import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.UserDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * SysUserUpdateDTO对象
 * 基础账户表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserUpdateDTO对象")
public class UserUpdateDTO extends BaseDTO<UserUpdateDTO, UserDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 账号id
     */
    @ApiModelProperty(value = "1-账号id")
    private Integer userId;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "2-邮箱")
    private String email;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "3-手机号")
    private String phone;
    /**
     * 登陆名
     */
    @ApiModelProperty(value = "4-登陆名")
    private String username;
    /**
     * 密码
     */
    @ApiModelProperty(value = "5-密码")
    private String password;
    /**
     * 用户类型（1.系统管理员；2,入驻药店；3.平台管理员；4.平台会员，5.入驻医生）
     */
    @ApiModelProperty(value = "6-用户类型（1.系统管理员；2,入驻药店；3.平台管理员；4.平台会员，5.入驻医生）")
    private Integer userType;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "7-租户id")
    private Integer tenantId;
    /**
     * 状态 1:enable, 0:disable, -1:deleted
     */
    @ApiModelProperty(value = "8-状态 1:enable, 0:disable, -1:deleted")
    private Integer status;

    private String realName;

    private String avatar;

    /**
     * 角色编号
     */
    @ApiModelProperty(value = "角色编号")
    private String selectedRoleIdsStr;

    /**
     * 职位编号
     */
    @ApiModelProperty(value = "职位编号")
    private String selectedPostIdsStr;

    /**
     * 部门编号
     */
    @ApiModelProperty(value = "部门编号")
    private String selectedDeptIdsStr;


    public List<String> getRoleIdList() {
        if (Fn.isEmpty(selectedRoleIdsStr)) {
            return null;
        }
        return Fn.str2List(selectedRoleIdsStr);
    }

    public List<String> getPostIdList() {
        if (Fn.isEmpty(selectedPostIdsStr)) {
            return null;
        }
        return Fn.str2List(selectedPostIdsStr);
    }

    public List<String> getDeptIdList() {
        if (Fn.isEmpty(selectedDeptIdsStr)) {
            return null;
        }
        return Fn.str2List(selectedDeptIdsStr);
    }

    public static Converter<UserUpdateDTO, UserDO> converter = new Converter<UserUpdateDTO, UserDO>() {
        @Override
        public UserDO doForward(UserUpdateDTO userUpdateDTO) {
            return WarpsUtils.copyTo(userUpdateDTO, UserDO.class);
        }

        @Override
        public UserUpdateDTO doBackward(UserDO user) {
            return WarpsUtils.copyTo(user, UserUpdateDTO.class);
        }
    };

    @Override
    public UserDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public UserUpdateDTO convertFor(UserDO user) {
        return converter.doBackward(user);
    }
}
