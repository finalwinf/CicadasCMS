
package com.cicadascms.service.system.logic.connect.wxmp;

import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.security.provider.ConnectAuthProvider;
import com.cicadascms.security.LoginUserDetails;
import com.cicadascms.data.domain.UserConnectionDO;
import com.cicadascms.service.system.logic.service.IUserConnectionService;
import com.cicadascms.service.system.logic.service.IUserDetailsService;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class WxMpAuthProvider extends ConnectAuthProvider<WxMpAuthToken, UserConnectionDO> {
    private IUserDetailsService userDetailsService;
    private IUserConnectionService userConnectionService;
    private WxMpService wxMpService;

    @Override
    protected UserConnectionDO connect(String code, boolean createUser) throws WxErrorException {
        UserConnectionDO userConnection;
        WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);

        //Jin 根据openid 或者 unionId 查询用户小程序绑定信息
        if (Fn.isNotEmpty(wxMpOAuth2AccessToken.getUnionId())) {
            userConnection = userConnectionService.findByOpenIdAndUnionId(wxMpOAuth2AccessToken.getOpenId(), wxMpOAuth2AccessToken.getUnionId(), "wxmp");
        } else {
            userConnection = userConnectionService.findByOpenId(wxMpOAuth2AccessToken.getOpenId(), "wxmp");
        }

        //判断是否需要绑定用户信息
        if (Fn.isNull(userConnection)) {
            if (createUser) {
                WxMpUser wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, "zh_CN");
                userConnection = userConnectionService.createUserConnection("wxmp", wxMpOAuth2AccessToken.getOpenId(), wxMpOAuth2AccessToken.getUnionId(), wxMpUser.getNickname(), wxMpUser.getHeadImgUrl());
                userConnection.setDisplayName(wxMpUser.getNickname());
                userConnection.setImageUrl(wxMpUser.getHeadImgUrl());
                userConnection.setExpireTime(wxMpOAuth2AccessToken.getExpiresIn());
                userConnection.setAccessToken(wxMpOAuth2AccessToken.getAccessToken());
                userConnection.setRefreshToken(wxMpOAuth2AccessToken.getRefreshToken());
                userConnectionService.updateById(userConnection);
            } else {
                throw new ServiceException("用户未关注公众号信息！");
            }
        }
        return userConnection;
    }


    @Override
    protected WxMpAuthToken process(Authentication authentication) throws Exception {
        WxMpAuthToken authenticationToken = (WxMpAuthToken) authentication;
        UserConnectionDO userConnection = connect(authenticationToken.getPrincipal().toString(), authenticationToken.isLoginFailCreate());
        LoginUserDetails user = (LoginUserDetails) userDetailsService.loadUserById(userConnection.getUserId());

        if (Fn.isNull(user)) {
            throw new InternalAuthenticationServiceException(authenticationToken.getPrincipal() + "登录失败 - 无法获取用户信息");
        }

        WxMpAuthToken wxMaAuthToken = new WxMpAuthToken(user, user.getAuthorities());
        wxMaAuthToken.setDetails(authenticationToken.getDetails());
        return wxMaAuthToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return WxMpAuthToken.class.isAssignableFrom(authentication);
    }

    @Autowired
    public void setUserDetailsService(IUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Autowired
    public void setUserConnectionService(IUserConnectionService userConnectionService) {
        this.userConnectionService = userConnectionService;
    }

    @Autowired
    public void setWxMpService(WxMpService wxMpService) {
        this.wxMpService = wxMpService;
    }
}
