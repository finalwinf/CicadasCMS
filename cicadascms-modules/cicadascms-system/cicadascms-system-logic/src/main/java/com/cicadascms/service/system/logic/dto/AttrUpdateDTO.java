package com.cicadascms.service.system.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AttrDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * AttrUpdateDTO对象
 * 附件
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="AttrUpdateDTO对象")
public class AttrUpdateDTO extends BaseDTO<AttrUpdateDTO, AttrDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
    * 附件分类
    */
    @ApiModelProperty(value = "2-附件分类" )
    private Integer attrClassId;
    /**
    * 附件名称
    */
    @ApiModelProperty(value = "3-附件名称" )
    private String attrName;
    /**
    * 附件存储类型
    */
    @ApiModelProperty(value = "4-附件存储类型" )
    private Integer attrStoreType;
    /**
    * 附件地址
    */
    @ApiModelProperty(value = "5-附件地址" )
    private String attrLocation;
    /**
    * 创建人
    */
    @ApiModelProperty(value = "6-创建人" )
    private Integer createBy;
    /**
    * 创建时间
    */
    @ApiModelProperty(value = "7-创建时间" )
    private LocalDateTime createTime;

    public static Converter<AttrUpdateDTO, AttrDO> converter = new Converter<AttrUpdateDTO, AttrDO>() {
        @Override
        public AttrDO doForward(AttrUpdateDTO attrUpdateDTO) {
            return WarpsUtils.copyTo(attrUpdateDTO, AttrDO.class);
        }

        @Override
        public AttrUpdateDTO doBackward(AttrDO attrDO) {
            return WarpsUtils.copyTo(attrDO, AttrUpdateDTO.class);
        }
    };

    @Override
    public AttrDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AttrUpdateDTO convertFor(AttrDO attrDO) {
        return converter.doBackward(attrDO);
    }
}
