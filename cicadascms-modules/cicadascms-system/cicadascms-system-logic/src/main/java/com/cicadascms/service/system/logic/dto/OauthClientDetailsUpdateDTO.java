package com.cicadascms.service.system.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.OauthClientDetailsDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * SysOauthClientDetailsUpdateDTO对象
 * oauth客户端
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="OauthClientDetailsUpdateDTO对象")
public class OauthClientDetailsUpdateDTO extends BaseDTO<OauthClientDetailsUpdateDTO, OauthClientDetailsDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 主键
    */
    @ApiModelProperty(value = "1-主键" )
    private Long id;
    /**
    * 应用标识
    */
    @ApiModelProperty(value = "2-应用标识" )
    private String clientId;
    /**
    * 资源限定串(逗号分割)
    */
    @ApiModelProperty(value = "3-资源限定串(逗号分割)" )
    private String resourceIds;
    /**
    * 应用密钥(bcyt) 加密
    */
    @ApiModelProperty(value = "4-应用密钥(bcyt) 加密" )
    private String clientSecret;
    /**
    * 应用密钥(明文)
    */
    @ApiModelProperty(value = "5-应用密钥(明文)" )
    private String clientSecretStr;
    /**
    * 范围
    */
    @ApiModelProperty(value = "6-范围" )
    private String scope;
    /**
    * 5种oauth授权方式(authorization_code,password,refresh_token,client_credentials)
    */
    @ApiModelProperty(value = "7-5种oauth授权方式(authorization_code,password,refresh_token,client_credentials)" )
    private String authorizedGrantTypes;
    /**
    * 回调地址
    */
    @ApiModelProperty(value = "8-回调地址" )
    private String webServerRedirectUri;
    /**
    * 权限
    */
    @ApiModelProperty(value = "9-权限" )
    private String authorities;
    /**
    * access_token有效期
    */
    @ApiModelProperty(value = "10-access_token有效期" )
    private Integer accessTokenValidity;
    /**
    * refresh_token有效期
    */
    @ApiModelProperty(value = "11-refresh_token有效期" )
    private Integer refreshTokenValidity;
    /**
    * {}
    */
    @ApiModelProperty(value = "12-{}" )
    private String additionalInformation;
    /**
    * 是否自动授权 是-true
    */
    @ApiModelProperty(value = "13-是否自动授权 是-true" )
    private String autoapprove;

    public static Converter<OauthClientDetailsUpdateDTO, OauthClientDetailsDO> converter = new Converter<OauthClientDetailsUpdateDTO, OauthClientDetailsDO>() {
        @Override
        public OauthClientDetailsDO doForward(OauthClientDetailsUpdateDTO oauthClientDetailsUpdateDTO) {
            return WarpsUtils.copyTo(oauthClientDetailsUpdateDTO, OauthClientDetailsDO.class);
        }

        @Override
        public OauthClientDetailsUpdateDTO doBackward(OauthClientDetailsDO oauthClientDetails) {
            return WarpsUtils.copyTo(oauthClientDetails, OauthClientDetailsUpdateDTO.class);
        }
    };

    @Override
    public OauthClientDetailsDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public OauthClientDetailsUpdateDTO convertFor(OauthClientDetailsDO oauthClientDetails) {
        return converter.doBackward(oauthClientDetails);
    }
}
