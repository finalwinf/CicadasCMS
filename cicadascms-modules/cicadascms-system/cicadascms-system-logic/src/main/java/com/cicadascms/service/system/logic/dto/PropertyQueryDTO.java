package com.cicadascms.service.system.logic.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.PropertyDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * PropertyQueryDTO对象
 *
 * </p>
 *
 * @author Jin
 * @since 2021-06-06
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="PropertyQueryDTO对象")
public class PropertyQueryDTO extends BaseDTO<PropertyQueryDTO, PropertyDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-排序字段")
    private String descs;

    @ApiModelProperty(value = "4-排序字段")
    private String ascs;

    private String name;
    private String propertyName;
    private String propertyValue;

    public Page<PropertyDO> page() {
        Page<PropertyDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<PropertyQueryDTO, PropertyDO> converter = new Converter<PropertyQueryDTO, PropertyDO>() {
        @Override
        public PropertyDO doForward(PropertyQueryDTO propertyQueryDTO) {
            return WarpsUtils.copyTo(propertyQueryDTO, PropertyDO.class);
        }

        @Override
        public PropertyQueryDTO doBackward(PropertyDO property) {
            return WarpsUtils.copyTo(property, PropertyQueryDTO.class);
        }
    };

    @Override
    public PropertyDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public PropertyQueryDTO convertFor(PropertyDO property) {
        return converter.doBackward(property);
    }
}
