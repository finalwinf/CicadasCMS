package com.cicadascms.service.system.logic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.RoleDO;
import com.cicadascms.data.domain.UserRoleDO;
import com.cicadascms.data.mapper.SysUserRoleMapper;
import com.cicadascms.service.system.logic.service.IRoleService;
import com.cicadascms.service.system.logic.service.IUserRoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户角色管理表 服务实现类
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
@Service
@AllArgsConstructor
public class UserRoleServiceImpl extends BaseService<SysUserRoleMapper, UserRoleDO> implements IUserRoleService {
    private final IRoleService roleService;

    @Override
    public List<UserRoleDO> findByUserId(Serializable uid) {
        return baseMapper.selectList(getLambdaQueryWrapper().eq(UserRoleDO::getUserId,uid));
    }

    @Override
    public void updateUserRole(Integer uid, List<String> roleIds) {
        if(Fn.isNotEmpty(roleIds)){

            LambdaQueryWrapper<UserRoleDO> lambdaQueryWrapper = getLambdaQueryWrapper().eq(UserRoleDO::getUserId, uid);
            Integer count = baseMapper.selectCount(lambdaQueryWrapper);

            if(count>0){
                baseMapper.delete(lambdaQueryWrapper);
            }

            roleIds.forEach(roleId->{
                RoleDO role =  roleService.getById(roleId);
                if(role!=null){
                    UserRoleDO userRoleDO = new UserRoleDO();
                    userRoleDO.setRoleId(role.getRoleId());
                    userRoleDO.setUserId(uid);
                    baseMapper.insert(userRoleDO);
                }
            });
        }
    }

    @Override
    protected String getCacheName() {
        return "userRoleCache";
    }
}
