package com.cicadascms.service.system.logic.dto;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.LogDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * LogQueryDTO对象
 * 日志表
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "LogQueryDTO对象")
public class LogQueryDTO extends BaseDTO<LogQueryDTO, LogDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-降序排序字段 多个字段用英文逗号隔开")
    private String descs;

    @ApiModelProperty(value = "4-升序排序字段  多个字段用英文逗号隔开")
    private String ascs;

    /**
     * 日志类型
     */
    @ApiModelProperty(value = "3-日志类型")
    private String type;
    /**
     * 日志标题
     */
    @ApiModelProperty(value = "4-日志标题")
    private String title;
    /**
     * 服务ID
     */
    @ApiModelProperty(value = "5-服务ID")
    private String clientId;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "6-创建者")
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "7-创建时间")
    private LocalDateTime createTime;
    /**
     * 操作IP地址
     */
    @ApiModelProperty(value = "8-操作IP地址")
    private String remoteAddr;
    /**
     * 用户代理
     */
    @ApiModelProperty(value = "9-用户代理")
    private String userAgent;
    /**
     * 请求URI
     */
    @ApiModelProperty(value = "10-请求URI")
    private String requestUri;
    /**
     * 操作方式
     */
    @ApiModelProperty(value = "11-操作方式")
    private String method;
    /**
     * 操作提交的数据
     */
    @ApiModelProperty(value = "12-操作提交的数据")
    private String params;
    /**
     * 执行时间
     */
    @ApiModelProperty(value = "13-执行时间")
    private String time;
    /**
     * 异常信息
     */
    @ApiModelProperty(value = "14-异常信息")
    private String exception;
    /**
     * 所属租户
     */
    @ApiModelProperty(value = "15-所属租户")
    private Integer tenantId;
    /**
     * 请求时长
     */
    @ApiModelProperty(value = "16-请求时长")
    private String processTime;

    public Page<LogDO> page() {
        Page<LogDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<LogQueryDTO, LogDO> converter = new Converter<LogQueryDTO, LogDO>() {
        @Override
        public LogDO doForward(LogQueryDTO logQueryDTO) {
            return WarpsUtils.copyTo(logQueryDTO, LogDO.class);
        }

        @Override
        public LogQueryDTO doBackward(LogDO log) {
            return WarpsUtils.copyTo(log, LogQueryDTO.class);
        }
    };

    @Override
    public LogDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public LogQueryDTO convertFor(LogDO log) {
        return converter.doBackward(log);
    }
}
