package com.cicadascms.service.system.logic.vo;

import com.cicadascms.common.dict.Dict;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * SysDictVO对象
 * </p>
 *
 * @author jin
 * @since 2020-04-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "SysDictVO对象", description = "字典表")
public class DictVO extends Dict {

}
