package com.cicadascms.service.system.logic.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.enums.JobStatus;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.QuartzJobLogDO;
import com.cicadascms.data.mapper.QuartzJobLogMapper;
import com.cicadascms.service.system.logic.dto.QuartzJobInputDTO;
import com.cicadascms.service.system.logic.dto.QuartzJobLogQueryDTO;
import com.cicadascms.service.system.logic.dto.QuartzJobQueryDTO;
import com.cicadascms.service.system.logic.dto.QuartzJobUpdateDTO;
import com.cicadascms.data.domain.QuartzJobDO;
import com.cicadascms.data.mapper.SysQuartzJobMapper;
import com.cicadascms.service.system.logic.service.IQuartzJobService;
import com.cicadascms.common.event.QuartzProcessEvent;
import com.cicadascms.service.system.logic.wrapper.QuartzJobWrapper;
import com.cicadascms.support.quartz.AbstractQuartzJobBean;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.redisson.executor.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 定时任务 服务实现类
 * </p>
 *
 * @author jin
 * @since 2020-04-29
 */
@Slf4j
@Service
public class QuartzJobServiceImpl extends BaseService<SysQuartzJobMapper, QuartzJobDO> implements IQuartzJobService {
    @Autowired
    private Scheduler scheduler;
    @Autowired
    private QuartzJobLogMapper quartzJobLogMapper;

    private static final String TRIGGER_IDENTITY = "trigger_";

    @Async
    @Order
    @EventListener(QuartzProcessEvent.class)
    @Override
    public void asyncSave(QuartzProcessEvent event) {
        Map<String, Object> logDO = (Map<String, Object>) event.getSource();
        ZoneId zoneId = ZoneId.systemDefault();
        QuartzJobLogDO quartzJobLogDO = new QuartzJobLogDO();
        quartzJobLogDO.setJobName((String) logDO.get("jobName"));

        Date fireTimeDate = (Date) logDO.get("fireTime");
        if (Fn.isNotNull(fireTimeDate)) {
            quartzJobLogDO.setFireTime(fireTimeDate.toInstant().atZone(zoneId).toLocalDateTime());
        }

        Date preFireTimeDate = (Date) logDO.get("preFireTime");
        if (Fn.isNotNull(preFireTimeDate)) {
            quartzJobLogDO.setPreFireTime(preFireTimeDate.toInstant().atZone(zoneId).toLocalDateTime());
        }

        Date nextFireTimeDate = (Date) logDO.get("nextFireTime");
        if (Fn.isNotNull(nextFireTimeDate)) {
            quartzJobLogDO.setNextFireTime(nextFireTimeDate.toInstant().atZone(zoneId).toLocalDateTime());
        }

        quartzJobLogDO.setProcessState((Integer) logDO.get("processState"));
        quartzJobLogMapper.insert(quartzJobLogDO);
    }

    @Override
    public R processLogPage(QuartzJobLogQueryDTO quartzJobLogQueryDTO) {
        IPage<QuartzJobLogDO> page = quartzJobLogMapper.selectPage(quartzJobLogQueryDTO.page(),
                new LambdaQueryWrapper<QuartzJobLogDO>().eq(QuartzJobLogDO::getJobName, quartzJobLogQueryDTO.getJobName())
        );
        return R.ok(page);
    }

    @Override
    public R page(QuartzJobQueryDTO quartzJobQueryDTO) {
        LambdaQueryWrapper<QuartzJobDO> lambdaQueryWrapper = getLambdaQueryWrapper();
        if (Fn.isNotEmpty(quartzJobQueryDTO.getJobName())) {
            lambdaQueryWrapper.like(QuartzJobDO::getJobName, quartzJobQueryDTO.getJobName());
        }
        IPage<QuartzJobDO> page = page(quartzJobQueryDTO.page(), lambdaQueryWrapper);
        return R.ok(QuartzJobWrapper.newBuilder().pageVO(page));
    }

    @Transactional
    @Override
    public R save(QuartzJobInputDTO quartzJobInputDTO) throws Exception {
        QuartzJobDO quartzJob = quartzJobInputDTO.convertToEntity();

        int count = count(getLambdaQueryWrapper()
                .eq(QuartzJobDO::getJobName, quartzJob.getJobName())
                .eq(QuartzJobDO::getJobGroup, quartzJob.getJobGroup())
        );
        if (count > 0) {
            throw new ServiceException("当前分组[" + quartzJob.getJobGroup() + "]已经存在 - " + quartzJob.getJobName());
        }

        schedulerJob(quartzJob);

        quartzJob.setTriggerState(JobStatus.ACQUIRED.getStatus());
        quartzJob.setOldJobGroup(quartzJob.getJobGroup());
        quartzJob.setOldJobName(quartzJob.getJobName());

        baseMapper.insert(quartzJob);

        return R.ok(true);
    }

    @Transactional
    @Override
    public R update(QuartzJobUpdateDTO quartzJobUpdateDTO) throws Exception {
        QuartzJobDO quartzJob = quartzJobUpdateDTO.convertToEntity();

        int count = count(getLambdaQueryWrapper()
                .notIn(QuartzJobDO::getId, quartzJob.getId())
                .eq(QuartzJobDO::getJobGroup, quartzJob.getJobGroup())
                .eq(QuartzJobDO::getJobName, quartzJob.getJobName()));
        if (count > 0) {
            throw new ServiceException("当前分组[" + quartzJob.getJobGroup() + "]已经存在 - " + quartzJob.getJobName());
        }

        scheduler.deleteJob(new JobKey(quartzJob.getOldJobName(), quartzJob.getOldJobGroup()));

        schedulerJob(quartzJob);

        quartzJob.setOldJobGroup(quartzJob.getJobGroup());
        quartzJob.setOldJobName(quartzJob.getJobName());

        baseMapper.updateById(quartzJob);

        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        QuartzJobDO quartzJob = baseMapper.selectById(id);
        return R.ok(quartzJob);
    }

    @Override
    public R deleteById(Serializable id) throws Exception {
        QuartzJobDO quartzJob = getById(id);
        TriggerKey triggerKey = TriggerKey.triggerKey(TRIGGER_IDENTITY + quartzJob.getJobName(), quartzJob.getJobGroup());
        scheduler.pauseTrigger(triggerKey);// 停止触发器
        scheduler.unscheduleJob(triggerKey);// 移除触发器
        scheduler.deleteJob(JobKey.jobKey(quartzJob.getJobName(), quartzJob.getJobGroup()));// 删除任务

        removeById(id);

        return R.ok(true);
    }

    @Override
    public R triggerJob(Serializable id) throws Exception {

        QuartzJobDO quartzJob = getById(id);
        scheduler.triggerJob(JobKey.jobKey(quartzJob.getJobName(), quartzJob.getJobGroup()));

        return R.ok(true);
    }

    @Override
    public R pauseJob(Serializable id) throws Exception {
        QuartzJobDO quartzJob = getById(id);

        scheduler.pauseJob(JobKey.jobKey(quartzJob.getJobName(), quartzJob.getJobGroup()));

        quartzJob.setTriggerState(JobStatus.PAUSED.getStatus());

        updateById(quartzJob);

        return R.ok(true);
    }

    @Override
    public R resumeJob(Serializable id) throws Exception {
        QuartzJobDO quartzJob = getById(id);
        scheduler.resumeJob(JobKey.jobKey(quartzJob.getJobName(), quartzJob.getJobGroup()));
        quartzJob.setTriggerState(JobStatus.RUNNING.getStatus());
        updateById(quartzJob);
        return R.ok(true);
    }


    @Override
    public void schedulerJob(QuartzJobDO job) {
        try {
            if (!CronExpression.isValidExpression(job.getCronExpression()))
                throw new IllegalArgumentException("cron 表达式不正确！- " + job.getCronExpression());

            boolean exists = scheduler.checkExists(JobKey.jobKey(job.getJobName(), job.getJobGroup())) || scheduler.checkExists(TriggerKey.triggerKey(TRIGGER_IDENTITY + job.getJobName(), job.getJobGroup()));
            if (exists) {
                log.info("任务已存在！ - {}", job.getJobName());
                return;
            }

            Class<AbstractQuartzJobBean> clazz = (Class<AbstractQuartzJobBean>) Class.forName(job.getJobClassName());
            log.info("开始调度任务 - {}", job.getJobName());

            Date nextFireTime = scheduler.scheduleJob(
                    JobBuilder
                            .newJob(clazz)
                            .withIdentity(job.getJobName(), job.getJobGroup()).withDescription(job
                            .getDescription())
                            .build()
                    , TriggerBuilder
                            .newTrigger()
                            .withIdentity(TRIGGER_IDENTITY + job.getJobName(), job.getJobGroup())
                            .startNow()
                            .withSchedule(CronScheduleBuilder.cronSchedule(job.getCronExpression().trim())).build());
            log.info("下次触发时间 {} - {}", DateUtil.formatDateTime(nextFireTime), job.getJobName());
        } catch (ClassNotFoundException | SchedulerException | IllegalArgumentException exception) {
            String message = exception instanceof ClassNotFoundException ? "执行类未找到！- " + job.getJobClassName() : exception.getMessage();
            throw new ServiceException(message);
        }
    }


    @Override
    protected String getCacheName() {
        return "quartzJobCache";
    }


}
