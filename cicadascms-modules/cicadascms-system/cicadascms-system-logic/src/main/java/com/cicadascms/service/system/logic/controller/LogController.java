package com.cicadascms.service.system.logic.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.LogDO;
import com.cicadascms.service.system.logic.dto.LogQueryDTO;
import com.cicadascms.service.system.logic.service.ILogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 日志表 控制器
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
@Api(tags = "日志管理接口")
@RestController
@RequestMapping("/system/log")
@AllArgsConstructor
public class LogController {
    private final ILogService logService;

    @ApiOperation(value = "日志列表接口")
    @GetMapping("/page")
    public R<Page<LogDO>> getPage(LogQueryDTO logQueryDTO) {
        return logService.page(logQueryDTO);
    }

    @ApiOperation(value = "日志详情接口")
    @GetMapping("/{id}")
    public R<LogDO> getById(@PathVariable Long id) {
        return logService.findById(id);
    }

    @ApiOperation(value = "日志删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return logService.deleteById(id);
    }


}
