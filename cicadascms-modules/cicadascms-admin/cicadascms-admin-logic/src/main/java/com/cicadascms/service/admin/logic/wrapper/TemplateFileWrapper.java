package com.cicadascms.service.admin.logic.wrapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.tree.TreeNode;
import com.cicadascms.service.admin.logic.vo.TemplateFileVO;

import java.io.File;
import java.util.List;

/**
 * ResourceFileWrapper
 *
 * @author Jin
 */
public class TemplateFileWrapper implements BaseWrapper<File, TemplateFileVO> {

    public static ResourceFileWrapper newBuilder() {
        return new ResourceFileWrapper();
    }

    @Override
    public TemplateFileVO entityVO(File entity) {
        return null;
    }

    @Override
    public <V extends TreeNode<V>> List<V> treeVO(List<V> list) {
        throw new IllegalArgumentException("此方法不可用！");
    }

    @Override
    public IPage<TemplateFileVO> pageVO(IPage<File> pages) {
        throw new IllegalArgumentException("此方法不可用！");
    }
}
