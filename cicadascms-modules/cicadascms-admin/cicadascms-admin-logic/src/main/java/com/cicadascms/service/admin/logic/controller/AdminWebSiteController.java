package com.cicadascms.service.admin.logic.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.service.admin.logic.dto.SiteInputDTO;
import com.cicadascms.service.admin.logic.dto.SiteQueryDTO;
import com.cicadascms.service.admin.logic.dto.SiteUpdateDTO;
import com.cicadascms.service.admin.logic.service.IAdminSiteService;
import com.cicadascms.service.admin.logic.vo.SiteVO;
import com.cicadascms.common.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


/**
 * <p>
 * 站点表 控制器
 * </p>
 *
 * @author jin
 * @since 2020-10-10
 */
@Api(tags = "A-站点管理接口")
@RestController
@RequestMapping("/admin/cms/site")
@AllArgsConstructor
public class AdminWebSiteController {
    private final IAdminSiteService siteService;

    @ApiOperation(value = "站点分页接口")
    @GetMapping("/page")
    public R<Page<SiteVO>> page(SiteQueryDTO siteQueryDTO) {
        return siteService.page(siteQueryDTO);
    }

    @ApiOperation(value = "站点保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid SiteInputDTO siteInputDTO) {
        return siteService.save(siteInputDTO);
    }

    @ApiOperation(value = "站点更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid SiteUpdateDTO siteUpdateDTO) {
        return siteService.update(siteUpdateDTO);
    }

    @ApiOperation(value = "站点详情接口")
    @GetMapping("/{id}")
    public R<SiteVO> getById(@PathVariable Long id) {
        return siteService.findById(id);
    }

    @ApiOperation(value = "站点删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return siteService.deleteById(id);
    }


}
