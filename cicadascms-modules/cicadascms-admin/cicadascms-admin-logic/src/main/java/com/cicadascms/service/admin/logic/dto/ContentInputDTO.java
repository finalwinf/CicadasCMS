package com.cicadascms.service.admin.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ContentDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * ContentInputDTO对象
 * 内容
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "InputContentDTO对象")
public class ContentInputDTO extends BaseDTO<ContentInputDTO, ContentDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 站点编号
     */
    @ApiModelProperty(value = "1-站点编号")
    private Integer siteId;
    /**
     * 栏目编号
     */
    @ApiModelProperty(value = "2-栏目编号")
    private Integer channelId;
    /**
     * 模型编号
     */
    @ApiModelProperty(value = "3-模型编号")
    private Integer modelId;
    /**
     * 标题
     */
    @ApiModelProperty(value = "4-标题")
    private String title;
    /**
     * 副标题
     */
    @ApiModelProperty(value = "5-副标题")
    private String subTitle;
    /**
     * 作者
     */
    @ApiModelProperty(value = "6-作者")
    private String author;
    /**
     * 页面关键字
     */
    @ApiModelProperty(value = "7-页面关键字")
    private Integer keywords;
    /**
     * 页面描述
     */
    @ApiModelProperty(value = "8-页面描述")
    private Integer description;
    /**
     * /**
     * 内容状态
     */
    @ApiModelProperty(value = "11-内容状态")
    private Integer state;
    /**
     * 来源
     */
    @ApiModelProperty(value = "12-来源")
    private String source;
    /**
     * 原文地址
     */
    @ApiModelProperty(value = "13-原文地址")
    private String sourceUrl;
    /**
     * 封面图片
     */
    @ApiModelProperty(value = "14-封面图片")
    private String thumb;
    /**
     * 浏览数量
     */
    @ApiModelProperty(value = "15-浏览数量")
    private Integer viewNum;
    /**
     * 价格
     */
    @ApiModelProperty(value = "16-价格")
    private Integer price;
    /**
     * 付费阅读
     */
    @ApiModelProperty(value = "17-付费阅读")
    private Boolean paidReading;
    /**
     * 付费阅读
     */
    @ApiModelProperty(value = "17-内容分页")
    private Integer pageTotal;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "18-扩展字段")
    private List<ModelFieldValueDTO> ext;

    public static Converter<ContentInputDTO, ContentDO> converter = new Converter<ContentInputDTO, ContentDO>() {
        @Override
        public ContentDO doForward(ContentInputDTO contentInputDTO) {
            return WarpsUtils.copyTo(contentInputDTO, ContentDO.class);
        }

        @Override
        public ContentInputDTO doBackward(ContentDO contentDO) {
            return WarpsUtils.copyTo(contentDO, ContentInputDTO.class);
        }
    };

    @Override
    public ContentDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ContentInputDTO convertFor(ContentDO contentDO) {
        return converter.doBackward(contentDO);
    }
}
