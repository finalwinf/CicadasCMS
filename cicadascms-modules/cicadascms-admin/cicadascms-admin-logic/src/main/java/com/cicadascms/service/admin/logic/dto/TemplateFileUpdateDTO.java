package com.cicadascms.service.admin.logic.dto;

import lombok.Data;
import lombok.ToString;

/**
 * TemplateFileUpdateDTO
 *
 * @author Jin
 */
@Data
@ToString
public class TemplateFileUpdateDTO {

    private String fileName;
    private String filePath;
    private String fileContent;

}
