package com.cicadascms.service.admin.logic.dto;

import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * <p>
 * ContentModelFieldValueDTO
 * 内容
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "ContentModelFieldValueDTO对象")
public class ModelFieldValueDTO {

    private static final long serialVersionUID = 1L;
    /**
     * 原文地址
     */
    @ApiModelProperty(value = "13-原文地址")
    private String fieldName;

    private ModelFieldValue fieldValue;
}
