package com.cicadascms.service.admin.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.SiteDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * SiteInputDTO对象
 * 站点表
 * </p>
 *
 * @author jin
 * @since 2020-10-12
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="InputSiteDTO对象")
public class SiteInputDTO extends BaseDTO<SiteInputDTO, SiteDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 站点名称
    */
    @NotEmpty(message = "站点名称不能为空！")
    @ApiModelProperty(value = "1-站点名称" )
    private String siteName;
    /**
    * http协议
    */
    @ApiModelProperty(value = "2-http协议" )
    private Integer httpProtocol;
    /**
    * 站点域名
    */
    @NotEmpty(message = "站点域名不能为空！")
    @ApiModelProperty(value = "3-站点域名" )
    private String domain;
    /**
    * 站点路径
    */
    @ApiModelProperty(value = "4-站点路径" )
    private String siteDir;
    /**
    * 站点状态
    */
    @ApiModelProperty(value = "5-站点状态" )
    private Boolean status;
    /**
    * 站点请求后缀
    */
    @ApiModelProperty(value = "6-站点请求后缀" )
    private Integer siteSuffix;
    /**
    * 是否默认站点
    */
    @ApiModelProperty(value = "7-是否默认站点" )
    private Boolean isDefault;
    /**
    * pc端模板目录
    */
    @ApiModelProperty(value = "8-pc端模板目录" )
    private String pcTemplateDir;
    /**
    * 移动端手机模板
    */
    @ApiModelProperty(value = "9-移动端手机模板" )
    private String mobileTemplateDir;

    public static Converter<SiteInputDTO, SiteDO> converter = new Converter<SiteInputDTO, SiteDO>() {
        @Override
        public SiteDO doForward(SiteInputDTO siteInputDTO) {
            return WarpsUtils.copyTo(siteInputDTO, SiteDO.class);
        }

        @Override
        public SiteInputDTO doBackward(SiteDO siteDO) {
            return WarpsUtils.copyTo(siteDO, SiteInputDTO.class);
        }
    };

    @Override
    public SiteDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public SiteInputDTO convertFor(SiteDO siteDO) {
        return converter.doBackward(siteDO);
    }
}
