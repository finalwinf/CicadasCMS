package com.cicadascms.service.admin.logic.service;


import com.cicadascms.service.admin.logic.dto.ResourceFileInputDTO;
import com.cicadascms.service.admin.logic.dto.ResourceFileUpdateDTO;
import com.cicadascms.service.admin.logic.vo.ResourceFileVO;

import java.util.List;

/**
 * <p>
 * 模管理 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IAdminResourceService {


    boolean saveResourceFile(ResourceFileInputDTO templateFileInputDTO);

    boolean updateResourceFile(ResourceFileUpdateDTO templateFileUpdateDTO);

    boolean deleteResourceFile(String filePath);

    List<ResourceFileVO> getResourceFileList(String filePath);

    ResourceFileVO find(String filePath);

}
