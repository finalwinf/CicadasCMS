package com.cicadascms.service.admin.logic.dto;

import lombok.Data;
import lombok.ToString;

/**
 * ResourceFileInputDTO
 *
 * @author Jin
 */
@Data
@ToString
public class ResourceFileInputDTO {

    private String fileName;
    private String filePath;
    private String fileContent;

}
