package com.cicadascms.service.admin.logic.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.service.admin.logic.dto.ChannelInputDTO;
import com.cicadascms.service.admin.logic.dto.ChannelQueryDTO;
import com.cicadascms.service.admin.logic.dto.ChannelUpdateDTO;
import com.cicadascms.service.admin.logic.service.IAdminChannelService;
import com.cicadascms.service.admin.logic.vo.ChannelVO;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.support.annotation.CurrentWebSite;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 栏目 控制器
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Api(tags = "A-栏目接口")
@RestController
@RequestMapping("/admin/cms/channel")
@AllArgsConstructor
public class AdminChannelController {
    private final IAdminChannelService channelService;

    @ApiOperation(value = "栏目树接口")
    @GetMapping("/tree")
    public R<ChannelVO> getTree(@CurrentWebSite SiteDO siteDO) {
        if (Fn.isNull(siteDO)) R.error("站点不存在！", false);
        return R.ok(channelService.getTree(siteDO.getSiteId()));
    }

    @ApiOperation(value = "栏目分页接口")
    @GetMapping("/list")
    public R<Page<ChannelVO>> page(ChannelQueryDTO channelQueryDTO) {
        return channelService.page(channelQueryDTO);
    }

    @ApiOperation(value = "栏目保存接口")
    @PostMapping
    public R<Boolean> save(@Valid @RequestBody ChannelInputDTO channelInputDTO) {
        return channelService.save(channelInputDTO);
    }

    @ApiOperation(value = "栏目更新接口")
    @PutMapping
    public R<Boolean> updateById(@Valid @RequestBody ChannelUpdateDTO channelUpdateDTO) {
        return channelService.update(channelUpdateDTO);
    }

    @ApiOperation(value = "栏目详情接口")
    @GetMapping("/{id}")
    public R<ChannelVO> getById(@PathVariable Integer id) {
        return channelService.findById(id);
    }

    @ApiOperation(value = "栏目删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Integer id) {
        return channelService.deleteById(id);
    }


}
