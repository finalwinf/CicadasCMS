/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 站点表
 * </p>
 *
 * @author jin
 * @since 2020-10-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_site")
public class SiteDO extends BaseDO {

    @TableId(value = "site_id", type = IdType.AUTO)
    private Integer siteId;

    /**
     * 站点名称
     */
    private String siteName;

    /**
     * http协议
     */
    private Integer httpProtocol;

    /**
     * 站点域名
     */
    private String domain;

    /**
     * 站点路径
     */
    private String siteDir;

    /**
     * 站点状态
     */
    private Boolean status;

    /**
     * 站点请求后缀
     */
    private Integer siteSuffix;

    /**
     * 是否默认站点
     */
    private Boolean isDefault;

    /**
     * pc端模板目录
     */
    private String pcTemplateDir;

    /**
     * 移动端手机模板
     */
    private String mobileTemplateDir;

}
