/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 定时任务执行日志
 * </p>
 *
 * @author Jin
 * @since 2021-06-08
 */
@TableName("sys_quartz_job_log")
public class QuartzJobLogDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 执行时间
     */
    private LocalDateTime fireTime;

    /**
     * 上次执行时间
     */
    private LocalDateTime preFireTime;

    /**
     * 下次执行时间
     */
    private LocalDateTime nextFireTime;

    /**
     * 执行状态
     */
    private Integer processState;

    /**
     * 执行日志
     */
    private String processLog;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public LocalDateTime getFireTime() {
        return fireTime;
    }

    public void setFireTime(LocalDateTime fireTime) {
        this.fireTime = fireTime;
    }

    public LocalDateTime getPreFireTime() {
        return preFireTime;
    }

    public void setPreFireTime(LocalDateTime preFireTime) {
        this.preFireTime = preFireTime;
    }

    public LocalDateTime getNextFireTime() {
        return nextFireTime;
    }

    public void setNextFireTime(LocalDateTime nextFireTime) {
        this.nextFireTime = nextFireTime;
    }

    public Integer getProcessState() {
        return processState;
    }

    public void setProcessState(Integer processState) {
        this.processState = processState;
    }

    public String getProcessLog() {
        return processLog;
    }

    public void setProcessLog(String processLog) {
        this.processLog = processLog;
    }

    @Override
    public String toString() {
        return "QuartzJobLogDO{" +
                "id=" + id +
                ", jobName=" + jobName +
                ", fireTime=" + fireTime +
                ", preFireTime=" + preFireTime +
                ", nextFireTime=" + nextFireTime +
                ", processState=" + processState +
                ", processLog=" + processLog +
                "}";
    }
}
