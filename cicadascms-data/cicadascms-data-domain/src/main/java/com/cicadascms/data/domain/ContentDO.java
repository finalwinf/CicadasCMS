/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 内容
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Getter
@Setter
@ToString
@TableName("cms_content")
public class ContentDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    /**
     * 内容编号
     */
    @TableId(value = "content_id", type = IdType.AUTO)
    private Integer contentId;

    /**
     * 站点编号
     */
    private Integer siteId;

    /**
     * 栏目编号
     */
    private Integer channelId;

    /**
     * 模型编号
     */
    private Integer modelId;

    /**
     * 标题
     */
    private String title;

    /**
     * 副标题
     */
    private String subTitle;

    /**
     * 作者
     */
    private String author;

    /**
     * 页面关键字
     */
    private String keywords;

    /**
     * 页面描述
     */
    private String description;

    /**
     * 内容状态
     */
    private Integer state;

    /**
     * 来源
     */
    private String source;

    /**
     * 原文地址
     */
    private String sourceUrl;

    /**
     * 封面图片
     */
    private String thumb;

    /**
     * 浏览数量
     */
    private Integer viewNum;

    /**
     * 价格
     */
    private Integer price;

    /**
     * 付费阅读
     */
    private Boolean paidReading;

    /**
     * 价格
     */
    private Integer pageTotal;

}
