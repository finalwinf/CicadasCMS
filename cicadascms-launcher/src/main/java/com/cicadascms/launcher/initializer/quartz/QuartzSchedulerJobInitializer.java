/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.initializer.quartz;

import com.cicadascms.common.enums.JobStatus;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.QuartzJobDO;
import com.cicadascms.service.system.logic.service.IQuartzJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Quartz 初始化
 *
 * @author Jin
 */
@Slf4j
@Component
@Configuration
public class QuartzSchedulerJobInitializer implements CommandLineRunner {

    private IQuartzJobService quartzJobService;

    @Autowired
    public void setQuartzJobService(IQuartzJobService quartzJobService) {
        this.quartzJobService = quartzJobService;
    }

    @Override
    public void run(String... args) {
        log.info("========[ 开始初始化调度任务 ]========");
        List<QuartzJobDO> quartzJobList = quartzJobService.list();
        if (Fn.isEmpty(quartzJobList)) {
            log.info("没有可启动的任务");
        } else {
            quartzJobList.stream().parallel().forEach(this::accept);
        }
        log.info("========[ 调度任务初始化完成 ]========\n");
    }

    private void accept(QuartzJobDO quartzJobDO) {
        try {
            if (Fn.notEqual(JobStatus.PAUSED, quartzJobDO.getTriggerState())) {
                quartzJobService.schedulerJob(quartzJobDO);
            }
        } catch (Exception e) {
            log.info("加载任务出错 - {}", e.getMessage());
            e.printStackTrace();
        }
    }
}
