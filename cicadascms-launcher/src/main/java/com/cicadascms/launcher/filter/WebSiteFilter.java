/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.filter;


import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.ControllerUtil;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.service.front.logic.service.IChannelService;
import com.cicadascms.service.front.logic.service.ISiteService;
import com.cicadascms.security.SecurityUrlProperties;
import com.cicadascms.support.website.WebSiteContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * WebSiteFilter
 *
 * @author Jin
 */
@Order(0)
@Component
@WebFilter(filterName = "WebSiteFilter", urlPatterns = {"/*"})
public class WebSiteFilter implements Filter {
    private ISiteService siteService;
    private IChannelService channelService;
    private SecurityUrlProperties securityUrlProperties;

    private final AntPathMatcher pathMatcher = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        String requestPath = request.getServletPath();

        if (pathMatcher.match("/favicon.ico", requestPath)) {
            request.getRequestDispatcher("/static/favicon.ico").forward(request, response);
            return;
        }

        if (pathMatcher.match("/system/**", requestPath) || pathMatcher.match("/oauth/**", requestPath)) {
            chain.doFilter(req, res);
            return;
        }

        String domain = ControllerUtil.getDomain(request);
        SiteDO siteDO = siteService.findByDomain(domain);

        if (Fn.isNull(siteDO)) {
            ChannelDO channelDO = channelService.findByDomain(domain);
            if (Fn.isNotNull(channelDO)) {
                siteDO = siteService.getById(channelDO.getSiteId());
                WebSiteContextHolder.set(siteDO);
                if (Fn.equal("/", requestPath)) {
                    request.getRequestDispatcher(channelDO.getChannelUrlPath()).forward(request, response);
                }
            } else {
                String siteId = request.getParameter("siteId");
                if (Fn.isEmpty(siteId)) {
                    siteDO = siteService.getDefaultSite();
                } else {
                    siteDO = siteService.getById(siteId);
                }
            }
        }

        if (Fn.isNull(siteDO)) {
            throw new FrontNotFoundException("站点不存在！");
        }

        WebSiteContextHolder.set(siteDO);
        if (pathMatcher.match("/search", requestPath))
            request.getRequestDispatcher("/" + siteDO.getSiteDir() + "/search").forward(request, response);
        chain.doFilter(req, res);
        WebSiteContextHolder.remove();

    }


    @Autowired
    public void setSiteService(ISiteService siteService) {
        this.siteService = siteService;
    }

    @Autowired
    public void setChannelService(IChannelService channelService) {
        this.channelService = channelService;
    }

    @Autowired
    public void setSecurityUrlProperties(SecurityUrlProperties securityUrlProperties) {
        this.securityUrlProperties = securityUrlProperties;
    }
}
