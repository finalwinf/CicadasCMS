/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.filter;

import com.cicadascms.common.func.Fn;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * GlobalSwaggerFilter 资源转发过滤器
 *
 * @author Jin
 */
@Component
@WebFilter(filterName = "GlobalSwaggerFilter", urlPatterns = {"/api-docs/*"})
public class SwaggerFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String requestUri = request.getRequestURI();
        if (Fn.equal(requestUri, "/api-docs/swagger-resources/configuration/security")) {
            request.getRequestDispatcher("/swagger-resources/configuration/security").forward(request, response);
            return;
        }
        if (Fn.equal(requestUri, "/api-docs/v2/api-docs")) {
            request.getRequestDispatcher("/v2/api-docs").forward(request, response);
            return;
        }
        if (Fn.equal(requestUri, "/api-docs/swagger-resources/configuration/ui")) {
            request.getRequestDispatcher("/swagger-resources/configuration/ui").forward(request, response);
            return;
        }
        if (Fn.equal(requestUri, "/api-docs/swagger-resources")) {
            request.getRequestDispatcher("/swagger-resources").forward(request, response);
            return;
        }
        chain.doFilter(req, res);
    }

}
