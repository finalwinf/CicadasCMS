/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.base;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.CacheUtils;
import com.cicadascms.common.utils.WarpsUtils;

import java.util.List;
import java.util.function.Supplier;

/**
 * BaseService
 *
 * @author Jin
 */
public abstract class BaseService<M extends BaseMapper<E>, E> extends ServiceImpl<M, E> {

    abstract protected String getCacheName();

    //=====================================================================================
    // 获取实体类缓存
    //=====================================================================================

    protected E getCacheForEntity(String key) {
        return CacheUtils.getCacheForObject(getCacheName(), key, getEntityClass());
    }


    protected E getCacheForEntity(String key, Supplier<E> supplier) {
        E entity = CacheUtils.getCacheForObject(getCacheName(), key, getEntityClass());
        if (Fn.isNull(entity)) {
            entity = supplier.get();
            //防止缓存存入null
            if (Fn.isNotNull(entity))
                CacheUtils.putCache(getCacheName(), key, entity);
        }
        return entity;
    }

    protected List<E> getCacheForEntityList(String key) {
        return CacheUtils.getCacheForList(getCacheName(), key, getEntityClass());
    }


    protected List<E> getCacheForEntityList(String key, Supplier<List<E>> supplier) {
        List<E> entityList = CacheUtils.getCacheForList(getCacheName(), key, getEntityClass());
        if (Fn.isEmpty(entityList)) {
            entityList = supplier.get();
            //防止缓存存入null
            if (Fn.isEmpty(entityList))
                CacheUtils.putCache(getCacheName(), key, entityList);
        }
        return entityList;
    }


    //=====================================================================================
    // 获取Obj缓存
    //=====================================================================================

    protected <T> T getCacheForObject(String key, Class<T> clazz) {
        return CacheUtils.getCacheForObject(getCacheName(), key, clazz);
    }


    protected <T> T getCacheForObject(String key, Supplier<T> supplier, Class<T> clazz) {
        T object = CacheUtils.getCacheForObject(getCacheName(), key, clazz);
        if (Fn.isNull(object)) {
            object = supplier.get();
            if (Fn.isNotNull(object))
                //防止缓存存入null
                CacheUtils.putCache(getCacheName(), key, object);
        }
        return object;
    }

    protected <T> List<T> getCacheForObjectList(String key, Class<T> clazz) {
        return CacheUtils.getCacheForList(getCacheName(), key, clazz);
    }


    protected <T> List<T> getCacheForObjectList(String key, Supplier<List<T>> supplier, Class<T> clazz) {
        List<T> entityList = CacheUtils.getCacheForList(getCacheName(), key, clazz);
        if (Fn.isEmpty(entityList)) {
            entityList = supplier.get();
            if (Fn.isEmpty(entityList))
                CacheUtils.putCache(getCacheName(), key, entityList);
        }
        return entityList;
    }

    //=====================================================================================
    // 移除缓存
    //=====================================================================================

    protected void removeCache(String key) {
        CacheUtils.clearCache(getCacheName(), key);
    }

    protected void removeCache(String... keys) {
        for (String key : keys) {
            CacheUtils.clearCache(getCacheName(), key);
        }
    }

    protected void removeAndSetCache(String key, Supplier<Object> supplier) {
        CacheUtils.clearCache(getCacheName(), key);
        CacheUtils.putCache(getCacheName(), key, supplier.get());
    }

    public void clearCache() {
        removeCache(getCacheName());
    }

    //=====================================================================================
    // 获取LambdaQueryWrapper
    //=====================================================================================


    public LambdaQueryWrapper<E> getLambdaQueryWrapper() {
        return new LambdaQueryWrapper<>();
    }

    public LambdaQueryWrapper<E> getLambdaQueryWrapper(E entity) {
        return new LambdaQueryWrapper<>(entity);
    }


    //=====================================================================================
    // 扩充查询方法
    //=====================================================================================

    public List<E> findAll() {
        return getCacheForEntityList("all", () -> baseMapper.selectList(Wrappers.emptyWrapper()));
    }

    //=====================================================================================
    // 其他
    //=====================================================================================

    protected <T> T copyTo(Object object, Class<T> clazz) {
        return WarpsUtils.copyTo(object, clazz);
    }
}
