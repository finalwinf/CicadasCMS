/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.utils;


import lombok.experimental.UtilityClass;

import java.text.DecimalFormat;

/**
 * AmountUtils
 *
 * @author Jin
 */
@UtilityClass
public class AmountUtils {

    /**
     * 分转元
     * @param amount
     * @return
     */
    public static String centToYuan(String amount){
        DecimalFormat df = new DecimalFormat("######0.00");
        Double d = Double.parseDouble(amount)/100;
        return df.format(d);
    }

    /**
     * 元转分
     * @param amount
     * @return
     */
    public static String yuanToCent(String amount){
        DecimalFormat df = new DecimalFormat("######0");
        Double d = Double.parseDouble(amount)*100;
        return df.format(d);
    }
}
