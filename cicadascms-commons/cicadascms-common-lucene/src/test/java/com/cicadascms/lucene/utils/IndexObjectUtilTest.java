package com.cicadascms.lucene.utils;

import com.cicadascms.lucene.model.TestIndexObject;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.highlight.Highlighter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * DocumentUtilTest
 *
 * @author Jin
 */
public class IndexObjectUtilTest {

    @Test
    public void test() {
        TestIndexObject testIndexObject = new TestIndexObject();
        testIndexObject.setAge(15);
        testIndexObject.setId("10001");
        testIndexObject.setName("lawrence");
        Map<String, Object> m = new HashMap<>();
        m.put("AAA", "aaa");
        m.put("BBB", "bbb");
        testIndexObject.setExt(m);
        Document document = IndexObjectUtil.indexObjectToDocument(testIndexObject);
//        testIndexObject = IndexObjectUtil.documentToIndexObject(new SmartChineseAnalyzer(), new Highlighter(null), document, 100, TestIndexObject.class);
//        Assertions.assertNotNull(testIndexObject);
    }
}