/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.lucene.annotation;


import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;

import java.lang.annotation.*;

/**
 * LuceneField
 *
 * @author Jin
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LuceneField {
    String name() default "";

    String pattern() default "";

    Class<? extends Field> field() default StoredField.class;

    Field.Store store() default Field.Store.YES;

    //是否需要检索的字段
    boolean isQueryField() default false;

    //是否扩展字段
    boolean isExtendField() default false;
}
