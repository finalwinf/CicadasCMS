/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.core.config;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.generator.InjectionConfig;

import java.util.Map;


/**
 * 自定义注入配置
 *
 * @author Jin
 */
public class MyFileGenerateConfig extends InjectionConfig {

    @Override
    public void initMap() {

    }

    @Override
    public Map<String, Object> prepareObjectMap(Map<String, Object> objectMap) {
        objectMap.putAll(getMap());
        String entityName = (String) objectMap.get("entity");
        if (entityName.endsWith("DO")) entityName = entityName.replace("DO", "");
        objectMap.put("nonPrefixEntityNameLower", StrUtil.lowerFirst(entityName));
        objectMap.put("nonPrefixEntityNameUpper", StrUtil.upperFirst(entityName));
        return objectMap;
    }
}
