#set($primaryKeyName='')
#foreach($field in ${table.fields})
    #if(${field.keyFlag})
        #set($primaryKeyName=$field.propertyName)
    #end
#end
<template>
    <el-card class="box-card">
        <div class="header">
            <el-input
              v-model="listQuery.${primaryKeyName}"
              placeholder="${primaryKeyName}"
              size="small"
              style="width: 300px;"
              class="filter-item"
              @keyup.enter.native="submitQuery"
            />
            <el-button
                    v-waves
                    class="filter-item"
                    type="primary"
                    size="small"
                    icon="el-icon-search"
                    @click="submitQuery"
            >搜索
            </el-button>
            <el-button
                    class="filter-item"
                    size="small"
                    icon="el-icon-delete"
                    type="default"
                    style="margin-left:0;"
                    @click="clearQuery"
            >清空
            </el-button>
        </div>
        <div class="header">
            <el-button
                    v-waves
                    class="filter-item"
                    type="primary"
                    icon="el-icon-plus"
                    @click="showCreateDialog"
            >新增$!{table.comment}</el-button>
        </div>
        <el-table
                :key="tableKey"
                v-loading="listLoading"
                :data="list"
                border
                fit
                highlight-current-row
                style="width: 100%;"
        >
            #foreach($field in ${table.fields})
                #if(${field.keyFlag})
                    <el-table-column label="#" min-width="80px" align="center">
                        <template slot-scope="{row}">
                            <span class="link-type">{{ row.${field.propertyName} }}</span>
                        </template>
                    </el-table-column>
                #else
                    <el-table-column label="${field.comment}" min-width="130px" align="center">
                        <template slot-scope="scope">
                            <span>{{ scope.row.${field.propertyName} }}</span>
                        </template>
                    </el-table-column>
                #end
            #end

            <el-table-column
                    fixed="right"
                    label="操作"
                    align="center"
                    width="310px"
                    class-name="small-padding fixed-width"
            >
                <template slot-scope="{row}">
                    <el-button
                            type="success"
                            icon="el-icon-view"
                            size="mini"
                            @click="showViewDialog(row.${primaryKeyName})"
                    >查看
                    </el-button>
                    <el-button
                            type="primary"
                            icon="el-icon-edit"
                            size="mini"
                            @click="showUpdateDialog(row.${primaryKeyName})"
                    >编辑
                    </el-button>
                    <el-button
                            type="danger"
                            icon="el-icon-delete"
                            size="mini"
                            @click="deleteHandle(row.${primaryKeyName})"
                    >删除
                    </el-button>
                </template>
            </el-table-column>
        </el-table>

        <el-dialog :title="titleMap[dialogStatus]" :lock-scroll="true" :visible.sync="dialogFormVisible">
            <div class="dialogBody">
                <el-scrollbar wrap-class="scrollbar-wrapper">
                    <el-form
                            ref="form"
                            :model="form"
                            :rules="rules"
                            label-width="80px"
                            :disabled="!(dialogStatus ==='update' || dialogStatus ==='create')"
                    >
                        <el-row>

                            #foreach($field in ${table.fields})
                                #if(!${field.keyFlag})
                                    #if($!{table.fields.size()}>5)
                                        <el-col :xs="24" :md="12">
                                            <el-form-item label="${field.comment}" prop="${field.propertyName}">
                                                <el-input v-model="form.${field.propertyName}"
                                                          placeholder="请输入${field.comment}"></el-input>
                                            </el-form-item>
                                        </el-col>
                                    #else
                                        <el-col :xs="24" :md="24" prop="${field.propertyName}">
                                            <el-form-item label="${field.comment}">
                                                <el-input v-model="form.${field.propertyName}"
                                                          placeholder="${field.comment}"></el-input>
                                            </el-form-item>
                                        </el-col>
                                    #end
                                #end
                            #end

                        </el-row>
                    </el-form>
                </el-scrollbar>
            </div>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogFormVisible = false">取消</el-button>
                <el-button type="primary" v-if="(dialogStatus ==='update' || dialogStatus ==='create')"
                           @click="saveHandle">保存
                </el-button>
            </div>
        </el-dialog>

        <pagination
                v-show="total>0"
                :total="total"
                :page.sync="listQuery.current"
                :limit.sync="listQuery.size"
                @pagination="getList"
        />
    </el-card>
</template>
<script>
    import {page, findById, save, deleteById, updateById} from "@/api/${nonPrefixEntityNameLower}";
    import waves from "@/directive/waves"; // waves directive
    import Pagination from "@/components/Pagination"; // secondary package based on el-pagination
    import {error} from "util";

    export default {
        name: "${entity}",
        components: {Pagination},
        directives: {waves},
        data() {
            return {
                tableKey: 0,
                list: null,
                total: 0,
                listLoading: true,
                listQuery: {
                    current: 1,
                    size: 12,
                   ${primaryKeyName}:undefined
                },
            rules: {
                #foreach($field in ${table.fields})
                    #if(!${field.keyFlag})
                        ${field.name}: [{required: true, trigger: "blur", message: "${field.comment}不能为空"}#if($!{table.fields.size()} != $foreach.count)],#else] #end
                    #end
                #end
            }
        ,dialogFormVisible: false,
         downloadLoading:false,
         titleMap:{
                update: "编辑",
                create:"新增",
                view:"查看"
         },
         dialogStatus: "",
          form:{}
        };
    },
    created(){
        this.getList();
    },
    methods: {
        getList(){
            this.listLoading = true;
            page(this.listQuery).then(response => {
                this.list = response.data.records;
                this.total = response.data.total;
                setTimeout(() => {
                    this.listLoading = false;
                }, 1.5 * 1000);
            });
        },
        submitQuery(){
            this.listQuery.current = 1;
            this.getList();
        },
        clearQuery(){
            this.listQuery.${primaryKeyName} = undefined;
            this.submitQuery();
        },
        showCreateDialog(){
            this.resetForm();
            this.dialogStatus = "create";
            this.dialogFormVisible = true;
            this.form = {};
        },
        showViewDialog(id){
            this.resetForm();
            this.dialogStatus = "view";
            findById(id).then(response => {
                this.form = response.data;
                setTimeout(() => {
                    this.listLoading = false;
                }, 1.5 * 1000);
            });
            this.dialogFormVisible = true;
        },
        showUpdateDialog(id){
            this.resetForm();
            this.dialogStatus = "update";
            findById(id).then(response => {
                this.form = response.data;
                setTimeout(() => {
                    this.listLoading = false;
                }, 1.5 * 1000);
            });
            this.dialogFormVisible = true;
        } ,
        deleteHandle(id){
            let me = this;
            this.$confirm("是否删除这条数据？?", "提示", {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning"
            }).then(() => {
                deleteById(id).then(() => {
                    me.getList();
                });
            });
        },
        #set($refs='$refs')
        saveHandle(){
            let me = this;
            me.${refs}.form.validate(valid => {
            if (valid) {
            this.downloadLoading = true;
            if (me.dialogStatus === "create") {
            save(this.form).then(() => {
            this.listLoading = false;
            me.dialogFormVisible = false;
            me.getList();
            });
            } else {
            updateById(this.form).then(() => {
            this.listLoading = false;
            me.dialogFormVisible = false;
            me.getList();
            });
            }
            } else {
            return false;
            }
            });
        },
        resetForm(){
            if (this.${refs}.form !== undefined) {
                this.${refs}.form.resetFields();
            }
        }
    }
    }
    ;
</script>

<style>
    .box-card {
        height: 100%;
        margin: 10px 8px;
        padding: 0;
    }

    .header {
        margin-bottom: 10px;
    }

    .dialogBody {
        height: 400px;
        overflow: hidden;
    }

    .scrollbar-wrapper {
        overflow-x: hidden !important;
    }

    .el-scrollbar__bar.is-vertical {
        right: 0px;
    }

    .el-scrollbar {
        height: 100%;
    }
</style>